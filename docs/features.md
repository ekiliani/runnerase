# Features

`runnerase` was developed as a central platform for interacting with RuNNer
through Python. In our research group, we realized that everyone has their own
set of Python and Bash scripts to set up calculations, track their progress,
analyze the results, and generate nice figures from the data. `runnerase`
offers convenient tools for all tasks along this way, in other words:

> runnerase enables you to manage your entire RuNNer workflow with Python.

The Atomic Simulation environment (ASE) was chosen as the underlying framework.
`runnerase` extends ASE with many features, including:

!!! tip "Features"
    - readers and writers for all RuNNer file formats.
    - `Runner`, an ASE calculator class for managing the RuNNer workflow.
    - `SymmetryFunction`/`SymmetryFunctionSet`, classes for generating and manipulating atom-centered symmetry functions.
    - A `plot`ting library for generating nice figures from RuNNer data.
    - storage classes for atom-centered symmetry function values, weights and scaling data of the atomic neural networks, the split between training and testing set, and the results of a RuNNer fit.
    - workflows for automatic training and LAMMPS-based simulations.
    - a dedicated ActiveLearning class.
