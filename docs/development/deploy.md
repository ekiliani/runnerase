This document describes the configuration of the build process for the
`runnerase` Python package that is distributed via `PyPi`.

## pyproject.toml, setup.cfg, and setup.py
The `runnerase` source directory contains three different files all associated
with configuring the build process of the package that can,
in the end, be installed via `pip`. This might be confusing at first, but
actually has a reason:

1. The `pyproject.toml` file is most important. It contains a section for the
   setup of the `build-system`, the project dependencies, entry point
   configuration, and the project metadata like its current version.
2. `setup.cfg` is currently only needed to setup the linting tools like pylint,
   flake8, and mypy. The goal is to transition in favor of `pyproject.toml` when
   these tools start supporting it.
3. `setup.py` is currently still needed by `setuptools` and `pip` to enable a
   local installation (i.e. `pip install -e .`) of the project. It will be
   dropped in the future.

Important resources for the sections of `pyproject.toml` are given below:

- [Packaging Python Projects](https://packaging.python.org/en/latest/tutorials/packaging-projects/)
- [Configuring setuptools using pyproject.toml](https://setuptools.pypa.io/en/stable/userguide/pyproject_config.html)
- [List of Classifiers](https://pypi.org/classifiers/)

## Deployment Process
The CI pipeline automatically deploys `runnerase` to two different package
registries:

- **all commits** except tags to `runnerase` will be deployed to the project's
package registry on Gitlab. This is handeled automatically by the CI pipeline as
long as the package builds.
- **tagged commits** are deployed to PyPi.
