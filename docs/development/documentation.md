The documentation of `runnerase` is built with [Mkdocs](https://www.mkdocs.org)
with a theme provided by
[Material for Mkdocs](https://squidfunk.github.io/mkdocs-material). These
projects are Python packages which take Markdown files as input and produce a
ready-to-publish static web page. Moreover, the package `mike` is used for
versioning.

For convenience, this process has been
automated for the `runnerase` manual via the CI pipeline. Take a look at
[this section](ci.md#Documentation) for details.

## Structure of the docs/ Directory

The master file containing all settings for the documentation is
[mkdocs.yml](https://gitlab.gwdg.de/runner/runner/-/blob/master/mkdocs.yml). See
the comments in the file for a description of all options. Most importantly,
this file contains a _nav_ section:

```yaml
nav:
  - Home: index.md
[...]
```

As one can see, this section puts the Markdown files in their correct location
on the web page. All Markdown files are contained in the _docs/_ folder. The
structure of this folder is completely arbitrary as long as the correct path is
given within the _nav_ section. The sub-folders are just meant for increased
readability.

### overrides Directory
The homepage of the documentation is not formatted with Markdown but has been
overridden with a custom .html page that can be found in the `overrides/`
directory within the `docs/` folder of this project.

### reference Directory
Most parts of the `runnerase` API are documented in the docstrings of the
relevant classes, functions, ... These parts of the source code are
automatically documented online using `mkdocstrings`. Therefore, most files in
the `reference/` directory only contain small statements specifying which part
of the source code will be automatically documented in any given file.

## Contributing

Contributing to the documentation is as easy as editing the existing Markdown
files or adding new ones to the docs/ folder. When you push the documentation to
the main branch of the git repository, the CI pipeline will automatically build
the new documentation and serve it under the GWDG web address.

#### Inspecting Local Modifications

Before committing a change, it is often beneficial to inspect your changes
locally. For that purpose, Mkdocs and mike both offer local development servers
which you can easily start on your computer.

  * Install the documentation dependencies, if you have not done so yet:

    ```sh
    pip install -e .[test,deploy]
    ```

  * Make your changes to the documentation.

  * If you only want to check out your current version of the documentation, use
    `mkdocs` to start a local server:

    ```sh
    mkdocs serve
    ```

  * Navigate to the displayed address (usually 127.0.0.1:8000) to see the
    documentation. Changes on the Markdown files automatically trigger a page
    update.

  * If you would like to see the whole documentation including previous
    versions, you can use the local development server offered by `mike`.

    ```sh
    mike serve
    ```
