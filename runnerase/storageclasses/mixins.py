#!/usr/bin/env python3
# encoding: utf-8
"""Implementation of mixin classes for storing RuNNer parameters and results.

Attributes
----------
ElementStorageMixin:
    A mixin for all storage containers storing data in element-wise fashion.
"""

from typing import Union, Iterator, Tuple, List, Dict

import numpy as np
from ase.data import chemical_symbols


class ElementStorageMixin:
    """Abstract mixin for storing element-specific RuNNer parameters/results.

    When constructing a neural network potential with RuNNer, one atomic neural
    network is built for every element in the system. As a result, many RuNNer
    parameters are element-specific.
    This mixin transforms any class into a storage container for
    element-specific data by
        - defining an abstract `data` container, i.e. a dictionary with
          str-np.ndarray pairs holding one numpy ndarray for each element.
          The keys are chemical symbols of elements.
        - defining magic methods like __iter__ and __setitem__ for convenient
          access back to that data storage.

    The storage of data in elementwise format is more efficient, as data can
    often be compressed into non-ragged numpy arrays.
    """

    def __init__(self) -> None:
        """Initialize the object."""
        # Data container for element - data pairs.
        self.data: Dict[str, np.ndarray] = {}

    def __iter__(self) -> Iterator[Tuple[str, np.ndarray]]:
        """Iterate over all key-value pairs in the `self.data` container."""
        for key, value in self.data.items():
            yield key, value

    def __len__(self) -> int:
        """Show the combined length of all stored element data arrays."""
        length = 0
        for value in self.data.values():
            shape: Tuple[int, ...] = value.shape
            length += shape[0]

        return length

    def __setitem__(
        self,
        key: Union[str, int],
        value: np.ndarray
    ) -> None:
        """Set one key-value pair in the self.data dictionary.

        Each key in self.data is supposed to be the chemical symbol of an
        element. Therefore, when an integer key is provided it is translated
        into the corresponding chemical symbol.

        Parameters
        ----------
        key:
            The dictionary key were `value` will be stored. Integer keys are
            translated into the corresponding chemical symbol.
        value:
            The element-specific data to be stored in the form of a numpy array.
        """
        if isinstance(key, int):
            symbol: str = chemical_symbols[key]
            key = symbol

        self.data[key] = value

    def __getitem__(self, key: Union[str, int]) -> np.ndarray:
        """Get the data associated with `key` in the self.data dictionary.

        The data can either be accessed by the atomic number or the chemical
        symbol.

        Parameters
        ----------
        key:
            Atomic number of chemical symbol of the desired `self.data`.
        """
        if isinstance(key, int):
            symbol: str = chemical_symbols[key]
            key = symbol

        return self.data[key]

    @property
    def elements(self) -> List[str]:
        """Show a list of elements for which data is available in storage."""
        return list(self.data.keys())
