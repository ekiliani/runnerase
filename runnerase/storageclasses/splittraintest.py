"""Implementation of classes for storing splitting between train and test set.

Attributes
----------
RunnerSplitTrainTest:
    Storage container for the train and test set split in RuNNer Mode 1.
"""

from typing import Optional, Union, List, TextIO

from runnerase.utils.iodecorators import reader
from runnerase.plot import RunnerSplitTrainTestPlots


class RunnerSplitTrainTest:
    """Storage container for the split between train and test set in RuNNer.

    In RuNNer Mode 1, the dataset presented to the program is separated into
    a training portion, presented to the neural networks for iteratively
    improving the weights, and a testing portion which is only used for
    evaluation.
    This class stores this data and enables to read it from Mode 1 output files.
    """

    def __init__(self, infile: Optional[Union[str, TextIO]] = None) -> None:
        """Initialize the object.

        Parameters
        ----------
        infile:
            If provided, data will be read from this fileobj.
        """
        self.train: List[int] = []
        self.test: List[int] = []

        # If given, read data from `infile`.
        if infile is not None:
            self.read(infile)

    def __repr__(self) -> str:
        """Show a string representation of the object."""
        return f'{self.__class__.__name__}(n_train={len(self.train)}, ' \
               + f'n_test={len(self.test)})'

    @reader
    def read(self, infile: TextIO) -> None:
        """Read RuNNer splitting data.

        Parameters
        ----------
        infile:
            Data will be read from this fileobj. The file should contain the
            stdout from a RuNNer Mode 1 run.
        """
        self.train = []
        self.test = []

        for line in infile:
            if 'Point is used for' in line:
                # In Python, indices start at 0, therefore we subtract 1.
                point_idx = int(line.split()[0]) - 1
                split_type = line.split()[5]

                if split_type == 'training':
                    self.train.append(point_idx)
                elif split_type == 'testing':
                    self.test.append(point_idx)

    @property
    def plot(self) -> RunnerSplitTrainTestPlots:
        """Create a plotting interface."""
        return RunnerSplitTrainTestPlots(self.train, self.test)
