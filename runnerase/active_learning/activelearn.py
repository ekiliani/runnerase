"""Implementation of the public class for active learning with Runner.

This modul contains an implementation of a `RunnerActiveLearn` object
for controlling and evaluating active learning cycles for HDNNPs.
"""

from typing import Optional, Any, Dict, List

from types import FunctionType
import os
import logging
from copy import deepcopy

import numpy as np

from ase.atoms import Atoms
from ase.calculators.calculator import CalculatorSetupError

from runnerase.calculators.runner import Runner
from runnerase.utils.submit import submit_slurm, slurm_barrier

from .potential import RunnerActiveLearnPotential

Settings = Dict[str, Any]


# pylint: disable=too-many-instance-attributes, too-many-arguments
# Active learning requires many parameters and values to keep track of.
class RunnerActiveLearn:
    """Controller and evaluation of active learning cycles with HDNNPs.

    A full active learning cycle, in the context of this class, consists
    of:

    - Training a certain number of HDNNPs on the same dataset in parallel.
    - Run parallel simulations based on the different potentials.
    - Recalculate all trajectories with all potentials (Runner Mode 3).
    - Analyze extrapolations and interpolations:
      - Compare predictions between HDNNPs.
      - Find and extract extrapolating environments.
      - Get recommendations for structures which are still needed in the
        dataset.

    Examples
    --------
    For a fully automatic run, the user has to define several parameters
    in advance:

    Load a training dataset:

    >>> dataset = read('input.data_all', index=':' format='runnerdata')

    Prepare a random number generator for reproducible runs:

    >>> rng = np.random.default_rng(42)

    Define settings for submission to the queue:

    >>> submit_settings = {
    >>>     'queue_name': 'myqueue',
    >>>     'num_cores': 1,
    >>>     'output_name': 'slurm_outputs/slurm-%j.out'
    >>> }

    Define three setse of training parameters (will train three separate
    potentials). In this case they are all trained with the intrinsic
    `train_hdnnp` function of runnerase:

    >>> training_setups = []
    >>> for _ in range(3):
    >>>     setup = {
    >>>         'training_function': train_hdnnp,
    >>>         'training_function_args': [],
    >>>         'training_function_kwargs': {}
    >>>     }
    >>>     training_setups.append(setup)

    Similarly, define setups for the simulations. Here, five simulation setups
    are created, but they only differ in the starting geometry:

    >>> simulation_setups: List[Dict[str, Any]] = []
    >>> for i in range(5):
    >>>
    >>>     struc_idx = int(rng.integers(1, len(dataset)))
    >>>     atoms = dataset[struc_idx]
    >>>     elements = get_elements([atoms], sort_by_appearance=True)
    >>>
    >>>     simulation_setups.append({
    >>>         'simulation_function': run_nvt_simulation,
    >>>         'simulation_function_args': [],
    >>>         'simulation_function_kwargs': {
    >>>             'initial_structure': atoms,
    >>>             'output_freq': 1,
    >>>             'elements': elements
    >>>         }
    >>>     })

    Create the active learning object:

    >>> ral = RunnerActiveLearn(
    >>>     directory='cycle1',
    >>>     dataset=dataset,
    >>>     simulation_setups=simulation_setups,
    >>>     rng=rng,
    >>>     runner_cmd='runner1 > PREFIX.out',
    >>>     submission_settings=submit_settings,
    >>>     training_setups=training_setups,
    >>>     submit=True,
    >>>     write_log=True
    >>> )

    Run one full training cycle:

    >>> ral.run_cycle()

    Load existing potentials and run only simulations:

    >>> ral.potentials_paths = ['path1', 'path2']
    >>> ral.load_potentials()
    >>> ral.run_md()

    Load existing calculations and run trajectory recalculation
    and subsequent analysis (all in parallel):

    >>> ral.load_simulations()
    >>> job_ids = ral.recalculate_trajectories()
    >>> slurm_barrier(job_ids)
    >>>
    >>> ral.load_recalculated()
    >>> job_ids = ral.analyze_results(plot=False)
    >>> slurm_barrier(job_ids)

    For one of the simulations, write the extrapolating environments
    around the atoms to xyz files. A 2.0 Angstrom margin is kept around
    the spherical environment so they can be equilibrated afterwards:

    >>> sim = ral.potentials[0].simulations[1]
    >>> sim.write_extrapolating_structures(
    >>>     filename='environments.xyz',
    >>>     whole_structure=False,
    >>>     margin=2.0
    >>> )

    """

    def __init__(
        self,
        directory: str,
        runner_cmd: str,
        rng: np.random.Generator,
        dataset: Optional[List[Atoms]] = None,
        training_setups: Optional[List[Settings]] = None,
        simulation_setups: Optional[List[Settings]] = None,
        submission_settings: Optional[Dict[str, Any]] = None,
        potential_paths: Optional[List[str]] = None,
        submit: bool = False,
        write_log: bool = True
    ):
        """Initialize the class.

        Parameters
        ----------
        directory:
            Name of the directory where the active learning cycle (all
            potentials, all simulations) will be stored.
        runner_cmd:
            The path to the Runner executable. Must respect the format:
            `<path_to_executable> > PREFIX.out`
        rng:
            An initialized random number generator for reproducibility between
            runs.
        dataset:
            The training dataset that will be used for training all potentials.
        training_setups:
            A list of dictionaries, each specifying the setup for one training
            run. The barebones structure of this dictionary is:

            >>> training_setups = {
            >>>     'training_function': train_hdnnp,
            >>>     'training_function_args': [],
            >>>     'training_function_kwargs': {},
            >>>     'settings': DEFAULT_TRAINING_PARAMETERS
            >>> }

            `DEFAULT_TRAINING_PARAMETERS` can be imported from the
            `runnerase.utils.defaults` module.
            `potential_paths` and `training_setups` cannot be
            specified at the same time, but one of them has to be present.
        simulation_setups:
            A list of dictionaries, each specifying the setup for one
            simulation. Each setup is run for every potential in
            `self.potentials`. The barebones structure of this dictionary is:

            >>> 'simulation_function': run_nvt_simulation,
            >>> 'simulation_function_args': [],
            >>> 'simulation_function_kwargs': {
            >>>     'initial_structure': atoms,
            >>>     'output_freq': 1,
            >>>     'elements': elements
            >>> }

            While the keys `output_freq` and `elements` are not strictly
            required, their use is highly encouraged. An output frequency
            of 1 makes the analysis of extrapolation warnings much more
            meaningful. The automatic detection of the right mapping of
            `elements` from RuNNer to LAMMPS can fail at times and should
            always be second-guessed by the user.
        submission_settings:
            A list of settings for submission to a queueing system.
            If self.submit is True, this must be given. The dictionary
            must at least contain the keys "num_cores" and "queue_name".
            Moreover, it is recommended to set the key-value pair
            `'output_name': 'slurm_outputs/slurm-%j.out'` and create the
            corresponding "slurm_outputs" directory. Otherwise, the
            training of multiple potentials and running of simulations
            can easily produce hundreds of log files in the root
            folder of the calculations.
        potential_paths:
            A list of paths to previously trained potentials. Each path
            must lead to a folder containing a "mode3" folder in turn.
            The potentials can be subsequently be loaded
            using `self.load_potentials`. `potential_paths` and
            `training_setups` cannot be specified at the same time, but
            one of them has to be present.
        submit:
            Whether to submit the calculation to the queue or not. If
            True, `submission_settings` must be specified as well.
        write_log:
            Whether to write a log file.

        """
        # Store settings.
        self.directory = directory
        self.runner_cmd = runner_cmd
        self.rng = rng
        self.dataset = dataset
        self.potential_paths = potential_paths
        self.submit = submit
        self.write_log = write_log

        # Storage container for results and input potentials.
        self.potentials: List[RunnerActiveLearnPotential] = []

        if write_log:
            self.prepare_log('activelearning.log')

        if self.submit and submission_settings is None:
            raise RuntimeError('Please provide settings for job submission.')

        # Initialize the potentials settings.
        if potential_paths is None and training_setups is None:
            raise RuntimeError('RunnerActiveLearn initialization requires '
                               'either a list of paths to pretrained '
                               'potentials or `num_potentials` and '
                               '`training_setups`.')

        if potential_paths is not None and training_setups is not None:
            raise RuntimeError('Pretrained potentials and training parameters '
                               'cannot be specified at the same time.')

        if potential_paths is not None:
            self.load_potentials()

        elif training_setups is not None:
            self.potential_paths = []
            self.training_setups = training_setups
            self.check_training_setups()

        if simulation_setups is not None:
            self.simulation_setups = simulation_setups
            self.check_simulation_setups()

        if submission_settings is not None:
            self.submission_settings = submission_settings
            self.check_submission_settings()

        if submit and submission_settings is None:
            raise CalculatorSetupError('When `submit` is True, '
                                       '`submission_settings` must be given.')

    def check_training_setups(self) -> None:
        """Check whether `self.training_setups` is correct.

        It must conform to the rules specified in the class description.
        """
        mandatory_keys = {
            'training_function': FunctionType,
            'training_function_args': list,
            'training_function_kwargs': dict,
            'settings': dict
        }

        mandatory_settings_keys = ['symfuns', 'filter', 'mode1',
                                   'mode2', 'mode3']

        for idx, setup, in enumerate(self.training_setups):
            for key, inst in mandatory_keys.items():
                # Are the relevant keys contained?
                if key not in setup:
                    raise CalculatorSetupError(
                        f'Training setup {idx} does not contain the '
                        f'key "{key}"'
                    )

                # Do the key values have the right type?
                if not isinstance(setup[key], inst):
                    raise CalculatorSetupError(
                        f'In training setup {idx}, the value of key "{key}" '
                        f'is not an instance of {inst}.'
                    )

            # Are all required training settings here?
            for setkey in mandatory_settings_keys:
                if setkey not in setup['settings']:
                    raise CalculatorSetupError(
                        'In training setup {idx}, the "settings" dictionary '
                        f'does not contain the mandatory key "{setkey}".'
                    )

    def check_simulation_setups(self) -> None:
        """Check whether `self.simulation_setups` is correct.

        It must conform to the rules specified in the class description.
        """
        mandatory_keys = {
            'simulation_function': FunctionType,
            'simulation_function_args': list,
            'simulation_function_kwargs': dict
        }

        mandatory_kwarg_keys = ['initial_structure']

        for idx, setup, in enumerate(self.simulation_setups):
            for key, inst in mandatory_keys.items():
                # Are the relevant keys contained?
                if key not in setup:
                    raise CalculatorSetupError(
                        f'Simulation setup {idx} does not contain the '
                        f'key "{key}"'
                    )

                # Do the key values have the right type?
                if not isinstance(setup[key], inst):
                    raise CalculatorSetupError(
                        f'In simulation setup {idx}, the value of key "{key}" '
                        f'is not an instance of {inst}.'
                    )

            # Are all required training settings here?
            for setkey in mandatory_kwarg_keys:
                if setkey not in setup['simulation_function_kwargs']:
                    raise CalculatorSetupError(
                        'In simulation setup {idx}, the '
                        '"simulation_function_kwargs" dictionary '
                        f'does not contain the mandatory key "{setkey}".'
                    )

    def check_submission_settings(self) -> None:
        """Check whether `self.submission_settings` is correct.

        It must conform to the rules specified in the class description.
        """
        mandatory_keys = {'queue_name': str, 'num_cores': int}

        for key, inst in mandatory_keys.items():
            # Are the relevant keys contained?
            if key not in self.submission_settings:
                raise CalculatorSetupError(
                    f'Submission settings do not contain the '
                    f'key "{key}"'
                )

            # Do the key values have the right type?
            if not isinstance(self.submission_settings[key], inst):
                raise CalculatorSetupError(
                    f'In submission settings, the value of key "{key}" '
                    f'is not an instance of {inst}.'
                )

    def prepare_log(self, filename):
        """Prepare a log file under the given `filename`."""
        self.logger = logging.getLogger(__name__)
        handler = logging.FileHandler(filename)

        # Define custom formatting.
        formatter = logging.Formatter(
            '%(asctime)s %(name)-12s %(levelname)-8s %(message)s'
        )
        handler.setFormatter(formatter)
        self.logger.addHandler(handler)

        # Only log anything of level INFO or higher.
        self.logger.setLevel(logging.INFO)

        # Do not propagate own logs to loggers on a higher level.
        self.logger.propagate = False

    def log(self, *args, **kwargs):
        """Write something to `self.logger`."""
        if self.write_log:
            self.logger.info(*args, **kwargs)

    def restart(self):
        """Restart a previous active learning cycle.

        Loads potentials, simulations, and recalculations.
        """
        self.load_potentials()
        self.load_simulations()
        self.load_recalculated()

    def load_potentials(self) -> None:
        """Load all potentials in self.potential_paths."""
        # Reset potentials.
        self.potentials = []

        if self.potential_paths is None:
            raise RuntimeError('Please specify at least one path to a '
                               'potential training folder (containing '
                               'a Mode 3 folder).')

        for pot_path in self.potential_paths:
            if not os.path.exists(pot_path):
                raise ValueError(f'Path "{pot_path}" does not exist')

            self.log('Loading potential %s', pot_path)
            pot = RunnerActiveLearnPotential(pot_path, command=self.runner_cmd)
            self.potentials.append(pot)

    def load_simulations(self):
        """Load simulation results of all potentials."""
        if len(self.potentials) == 0:
            self.load_potentials()

        for pot in self.potentials:
            self.log('Loading simulations for potential %s', pot.directory)
            pot.read_all_simulations()

    def load_recalculated(self):
        """Load the traj recalculations of all potentials and simulations."""
        if len(self.potentials[0].simulations) == 0:
            self.load_simulations()

        for pot in self.potentials:
            for idx_sim, sim in enumerate(pot.simulations):
                self.log('Loading potential %s, simulation %i',
                         pot.directory, idx_sim)
                sim.read_all_recalculations()

    def run_cycle(self):
        """Run a full active learning cycle.

        This consists of potential training, simulations,
        trajectory recalculation, analysis and final reporting.
        """
        self.log('Running a full active learning cycle')

        if len(self.potentials) == 0:
            job_ids = self.train()

            if self.submit:
                slurm_barrier(job_ids)

        # Reload all potentials here (will not be loaded if run in the queue).
        self.load_potentials()

        # Run multiple MD simulations for each potential.
        job_ids = self.run_md()

        if self.submit:
            slurm_barrier(job_ids)

        self.load_simulations()

        # Recalculate MD trajectories with the other potentials.
        self.recalculate_trajectories()

        if self.submit:
            slurm_barrier(job_ids)

        # Analyze the results and print final report.
        self.load_recalculated()
        self.analyze_results()
        self.collect_reports()

    def train(self) -> List[int]:
        """Train multiple potentials with the given settings."""
        self.log('Training potentials')

        num_potentials = len(self.training_setups)

        job_ids = []
        for idx_pot in range(num_potentials):
            # Get the parameters for this potential.
            parameters = self.training_setups[idx_pot]
            training_function = parameters['training_function']
            args = parameters['training_function_args']
            kwargs = parameters['training_function_kwargs']
            settings = parameters['settings']

            # Update settings with reproducible seed.
            seed = int(self.rng.integers(1, 1000))
            settings['mode1']['random_seed'] = seed

            # Initialize fit label.
            fitdir = os.path.join(self.directory, f'pot_{idx_pot}')

            # Create the calculator object.
            calc = Runner(
                command=self.runner_cmd,
                dataset=self.dataset,
                label=f'{fitdir}/mode1/mode1'
            )
            calc.profile.prefix = calc.prefix

            # Save the potential.
            self.potential_paths.append(fitdir)  # type: ignore

            # Train the potential.
            if self.submit:

                job_id = submit_slurm(
                    training_function,
                    calc,
                    settings,
                    *args,
                    **self.submission_settings,
                    **kwargs,
                )

                self.log(
                    'Training potential %s in Slurm job %i (%i/%i)',
                    fitdir,
                    job_id,
                    idx_pot + 1,
                    num_potentials
                )

                job_ids.append(job_id)

            else:
                self.log(
                    'Training potential %s (%i/%i)',
                    fitdir,
                    idx_pot + 1,
                    num_potentials
                )

                try:
                    training_function(calc, *args, **kwargs)
                except RuntimeError as err:
                    self.log(err)

        return job_ids

    def run_md(self) -> List[int]:
        """Spawn MD simulations for each potential in `self.potentials`."""
        self.log('Running simulations')

        job_ids = []
        for pot in self.potentials:
            for idx_sim, sim in enumerate(self.simulation_setups):

                sim_function = sim['simulation_function']
                args = sim['simulation_function_args'] or []
                kwargs = sim['simulation_function_kwargs'] or {}

                if kwargs is None:
                    kwargs = {}

                dirname = os.path.join(pot.simulation_path, str(idx_sim))

                if self.submit:

                    job_id = submit_slurm(
                        sim_function,
                        calc=pot.calc,
                        rng=self.rng,
                        directory=dirname,
                        *args,
                        **kwargs,
                        **self.submission_settings
                    )

                    self.log(
                        'Running simulation %s in Slum job %i',
                        dirname,
                        job_id
                    )

                    job_ids.append(job_id)

                else:
                    self.log('Running simulation %s', dirname)
                    try:
                        sim_function(
                            calc=pot.calc,
                            rng=self.rng,
                            directory=dirname,
                            *args,
                            **kwargs
                        )
                    except RuntimeError as err:
                        self.log(err)

        return job_ids

    def recalculate_trajectories(self):
        """Recalculate all trajs with all potent. in `self.potential`."""
        self.log('Recalculating trajectories')

        if len(self.potentials) == 0:
            raise RuntimeError('Please specify at least one potential to '
                               'recalculate trajectories.')

        job_ids = []
        for parent_pot in self.potentials:
            for sim in parent_pot.simulations:
                for idx_recalc, recalc_pot in enumerate(self.potentials):
                    # Define the calculation label.
                    label = os.path.join(
                        sim.recalculation_path,
                        str(idx_recalc),
                        'mode3'
                    )

                    # Create a new calculator with empty results.
                    calc = deepcopy(recalc_pot.calc)
                    calc.command = self.runner_cmd
                    calc.label = label
                    calc.reset()
                    calc.dataset = sim.traj

                    # sim.traj can have length zero if the simulation stopped
                    # immediately.
                    if len(sim.traj) == 0:
                        continue

                    # Run Mode 3 to recalculate all structures along traj.
                    if self.submit:

                        job_id = submit_slurm(
                            calc.run,
                            mode=3,
                            **self.submission_settings
                        )

                        self.log(
                            'Submitting recalculation %s in Slurm job %i',
                            label,
                            job_id
                        )

                        job_ids.append(job_id)

                    else:
                        self.log('Running recalculation %s', label)

                        try:
                            calc.run(mode=3)
                        except RuntimeError as err:
                            self.log(err)

        return job_ids

    def analyze_results(self, plot: bool = True) -> List[int]:
        """Analyze the results of all simulations and recalculations."""
        self.log('Analyzing results')

        job_ids = []
        for idx_pot, pot in enumerate(self.potentials):
            for idx_sim, sim in enumerate(pot.simulations):

                if self.submit:

                    if self.submission_settings is None:
                        raise ValueError('Please specify submission settings '
                                         'before submitting.')

                    self.log(
                        'Submitting analysis for pot %i/%i, simulation %i/%i',
                        idx_pot,
                        len(self.potentials),
                        idx_sim,
                        len(pot.simulations)
                    )

                    # Analyze extrapolations.
                    job_id = submit_slurm(
                        sim.analyze_extrapolations,
                        **self.submission_settings
                    )

                    job_ids.append(job_id)

                    # Analyze interpolations.
                    job_id = submit_slurm(
                        sim.analyze_recalculations,
                        plot=plot,
                        **self.submission_settings
                    )

                    job_ids.append(job_id)

                else:
                    self.log(
                        'Analyzing pot %i/%i, simulation %i/%i',
                        idx_pot,
                        len(self.potentials),
                        idx_sim,
                        len(pot.simulations)
                    )

                    sim.analyze_extrapolations()
                    sim.analyze_recalculations(plot=plot)

        return job_ids

    def collect_reports(self):
        """Create a final report about all extrapolations and interpolations.

        This also gives recommendations about the structures which should be
        added to the training dataset.
        """
        self.log('Generating final report')
        return 0
