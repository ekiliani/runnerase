"""Decorators for IO functions.

The decorators in this module enable any function that does file-based I/O
to be called with both a string or an already opened TextIO object.
If they are given a string, the corresponding file is simply opened and
then the TextIO object is passed on to the actual function.
"""

from typing import Callable, Any, Union, Dict, TextIO

import functools

# Custom type specifications for specific I/O function signatures.
IOFunction = Callable[..., Any]

IOFunctionDecorated = Callable[..., Any]


def writer(
    function: IOFunction
) -> IOFunctionDecorated:
    """Decorate any `function` to write to both `str` path and TextIO."""
    return _io_decorator('w', function)


def reader(
    function: IOFunction
) -> IOFunctionDecorated:
    """Decorate any `function` to read from both `str` path and TextIO."""
    return _io_decorator('r', function)


def _io_decorator(
    mode: str,
    function: IOFunction
) -> IOFunctionDecorated:
    """Transform `str` path arguments of I/O functions to TextIO.

    This function enables any I/O function to work with both a `str` argument
    specifying the path to read from/write to, and an already opened
    TextIO object.

    The routine works for both singular functions (`IOFunction` signature) and
    class methods (`IOClassMethod` signature).
    The routine only works when the first argument of the function (the first
    argument after `self` in the case of a class method) is either a path or
    a TextIO object.

    Arguments
    ---------
    mode:
        Whether the file should be read (='r') or written (='w').
    function:
        The I/O function to be transformed.

    Returns
    -------
    iofunction:
    """
    @functools.wraps(function)
    def iofunction(
        path_to_file: Union[str, TextIO],
        *args: Dict[Any, Any],
        **kwargs: Dict[Any, Any]
    ) -> IOFunctionDecorated:
        """Wrap `function` in a context manager that opens `path_to_file`."""
        if isinstance(path_to_file, str):
            with open(path_to_file, mode, encoding='utf8') as infile:
                obj: IOFunctionDecorated = function(infile, *args, **kwargs)

        else:
            obj = function(path_to_file, *args, **kwargs)

        return obj

    @functools.wraps(function)
    def iofunction_class(
        class_instance: object,
        path_to_file: Union[str, TextIO],
        *args: Dict[Any, Any],
        **kwargs: Dict[Any, Any]
    ) -> IOFunctionDecorated:
        """Wrap `function` in a context manager that opens `path_to_file`.

        In contrast to `iofunction`, this function works for class methods.
        Class methods usually carry `self` as the first function argument,
        which is extracted here as `class_instance`.
        """
        if isinstance(path_to_file, str):
            with open(path_to_file, mode, encoding='utf8') as infile:
                obj: IOFunctionDecorated = function(class_instance, infile,
                                                    *args, **kwargs)

        else:
            obj = function(class_instance, path_to_file, *args, **kwargs)

        return obj

    if '.' in str(function):
        return iofunction_class

    return iofunction
