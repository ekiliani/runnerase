#!/usr/bin/env python3
# encoding: utf-8
"""Basic math functions."""

import numpy as np


def calc_rmse(x: np.ndarray, y: np.ndarray) -> float:
    """Calculate the root mean square error between `y` and `x`."""
    return np.sqrt(((y - x)**2).mean())


def calc_mae(x: np.ndarray, y: np.ndarray) -> float:
    """Calculate the mean average error between `y` and `x`."""
    return abs(y - x).mean()
