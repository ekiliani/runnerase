#!/usr/bin/env python3
# encoding: utf-8
"""Check progress of the different RuNNer modes."""

from typing import TextIO

from runnerase.storageclasses import RunnerFitResults
from runnerase.utils.iodecorators import reader


@reader
def check_status_mode1(infile: TextIO):
    """Return the number of processed structures in Mode 1."""
    status = 0

    for line in infile:
        if 'Point is used for' in line:
            status = int(line.split()[0])

    return status


@reader
def check_status_mode2(infile: TextIO):
    """Return the number of finished epochs in Mode 2."""
    fitresults = RunnerFitResults(infile)

    # Subtract -1 for 0th epoch.
    return max(0, len(fitresults.epochs) - 1)


@reader
def check_status_mode3(infile: TextIO):
    """Return the number of processed structures in Mode 3."""
    status = 0

    for line in infile:
        if 'NNconfiguration' in line:
            status = int(line.split()[1])

    return status
