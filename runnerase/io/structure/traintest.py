"""Implementation of readers and writers for traindata and testdata files."""

from typing import Union, List, TextIO

import numpy as np

from ase.atoms import Atoms
from ase.utils import reader, writer
from ase.units import Hartree, Bohr
from ase.data import atomic_numbers

from .tempatoms import TempAtoms


@writer
def write_trainteststruct(
    outfile: TextIO,
    images: Union[Atoms, List[Atoms]],
    index: Union[int, slice] = slice(0, None),
    fmt: str = '16.10f',
    input_units: str = 'si'
) -> None:
    """Write a series of ASE Atoms to trainstruct.data / teststruct.data format.

    Parameters
    ----------
    outfile:
        The fileobj where the data will be written.
    images:
        List of ASE `Atoms` objects.
    index:
        Only the selection of `images` given by `index` will be written.
    fmt:
        A format specifier for float values.
    input_units:
        The units within `images`. Can be 'si' or 'atomic'. All data will
        automatically be converted to atomic units.
    """
    # Filter the images which should be printed according to `index`.
    if isinstance(index, (int, slice)):
        images = images[index]
    else:
        images = [images[i] for i in index]

    for idx_atoms, atoms in enumerate(images):

        # Transform into a TempAtoms object and do unit conversion, if needed.
        tempatoms = TempAtoms(atoms)
        tempatoms.convert(input_units=input_units, output_units='atomic')

        # Write structure index. White space at the end is important.
        outfile.write(f'{idx_atoms + 1:8} ')

        # Write lattice vectors for periodic structures.
        if any(tempatoms.pbc):
            outfile.write('T\n')
            for vector in tempatoms.cell:
                outfile.write(f'{vector[0]:{fmt}} {vector[1]:{fmt}} '
                              f'{vector[2]:{fmt}} \n')
        else:
            outfile.write('F\n')

        # Write atomic data to file.
        for xyz, element, charge, atomenergy, force_xyz in tempatoms:
            atomic_number = atomic_numbers[element]
            outfile.write(f'{atomic_number:4d} {xyz[0]:{fmt}} {xyz[1]:{fmt}} '
                          f'{xyz[2]:{fmt}} '
                          f'{charge:{fmt}} {atomenergy:{fmt}} '
                          f'{force_xyz[0]:{fmt}} {force_xyz[1]:{fmt}} '
                          f'{force_xyz[2]:{fmt}}\n')


@reader
def read_traintestpoints(
    infile: TextIO,
    input_units: str = 'atomic',
    output_units: str = 'si'
) -> np.ndarray:
    """Read RuNNer trainpoint.XXXXXX.out / testpoint.XXXXXX.out.

    Parameters
    ----------
    infile:
        The fileobj where the data will be read.
    input_units:
        The units within `images`. Can be 'si' or 'atomic'. All data will
        automatically be converted to `output_units`.
    output_units:
        The desired units in `data`. Can be 'si' or 'atomic'.

    Returns
    -------
    data:
        An array holding the following columns: image ID, number of atoms,
        reference energy, neural network energy, difference between ref. and
        neural network energy.
    """
    # The first row holds the column names.
    data: np.ndarray = np.loadtxt(infile, skiprows=1)

    # Unit conversion.
    if input_units == 'atomic' and output_units == 'si':
        data[:, 2:] *= Hartree

    elif input_units == 'si' and output_units == 'atomic':
        data[:, 2:] /= Hartree

    return data


@reader
def read_traintestforces(
    infile: TextIO,
    input_units: str = 'atomic',
    output_units: str = 'si'
) -> np.ndarray:
    """Read RuNNer trainforces.XXXXXX.our / testpoint.XXXXXX.out.

    Parameters
    ----------
    infile:
        The fileobj where the data will be read.
    input_units:
        The units within `images`. Can be 'si' or 'atomic'. All data will
        automatically be converted to `output_units`.
    output_units:
        The desired units in `data`. Can be 'si' or 'atomic'.

    Returns
    -------
    data:
        An array holding the following columns: image ID, atom ID, reference
        force x, reference force y, reference force z, neural network force x,
        neural network force y, neural network force z.
    """
    # The first row holds the column names.
    data: np.ndarray = np.loadtxt(infile, skiprows=1)

    # Unit conversion.
    if input_units == 'atomic' and output_units == 'si':
        data[:, 2:] *= Hartree * Bohr

    elif input_units == 'si' and output_units == 'atomic':
        data[:, 2:] /= Hartree * Bohr

    return data


@writer
def write_traintestforces(
    outfile: TextIO,
    images: Union[Atoms, List[Atoms]],
    index: Union[int, slice, List[int]] = slice(0, None),
    fmt: str = '16.10f',
    input_units: str = 'si'
) -> None:
    """Write a series of ASE Atoms to trainforces.data / testforces.data format.

    Parameters
    ----------
    outfile:
        The fileobj where the data will be written.
    images:
        List of ASE `Atoms` objects.
    index:
        Only the selection of `images` given by `index` will be written.
    fmt:
        A format specifier for float values.
    input_units:
        The units within `images`. Can be 'si' or 'atomic'. All data will
        automatically be converted to atomic units.
    """
    # Filter the images which should be printed according to `index`.
    if isinstance(index, (int, slice)):
        images = images[index]
    else:
        images = [images[i] for i in index]

    for idx_atoms, atoms in enumerate(images):

        # Transform into a TempAtoms object and do unit conversion, if needed.
        tempatoms = TempAtoms(atoms)
        tempatoms.convert(input_units=input_units, output_units='atomic')

        # Write structure index. White space at the end is important.
        outfile.write(f'{idx_atoms + 1:8}\n')

        # Write atomic data to file.
        for _, _, _, _, force_xyz in tempatoms:
            outfile.write(f'{force_xyz[0]:{fmt}} {force_xyz[1]:{fmt}} '
                          f'{force_xyz[2]:{fmt}}\n')
