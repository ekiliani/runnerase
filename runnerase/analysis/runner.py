"""RuNNer fit analysis class."""

from typing import Dict, Any, List, Tuple, Optional

from collections import defaultdict
import numpy as np

from ase.atoms import Atoms

from runnerase.utils.math import calc_rmse, calc_mae
from runnerase.utils.atoms import (get_elements,
                                   binary_composition,
                                   optimize_atomic_energies)


# pylint: disable=too-many-instance-attributes
# Reason: the combinatorics mean many instance attributes: Energy or forces,
# per atom or not per atom, prediction or reference, ... the properties
# make these things easy to access.
class RunnerAnalysis:
    """A analysis interface for the Runner calculator.

    This class provides many useful analysis functions for HDNNPs.
    It has two peculiarities:
    1. The `mask` array allows the user to filter structures from the
       dataset. Only those structures for which `self.mask` is True are
       considered for an analysis. The dataset is never altered, allowing
       the application of different masks without reloading the
       RunnerAnalysis object (but the internal array has to cleared once).
    2. It contains several internal arrays (e.g. _energy_reference, _natoms)
       for storing results from a calculation. When a property is requested for
       the first time (e.g. for calculating the RMSE values), the values
       of the property are extracted and stored in the internal array.
       Thus, they do not have to be recomputed when they are requested again
       (e.g. for plotting). This increases performance a lot.
    """

    def __init__(
        self,
        dataset: List[Atoms],
        results: Dict[str, Any]
    ) -> None:
        """Initialize the class.

        Parameters
        ----------
        dataset:
            The training dataset in the Runner object.
        results:
            All results that were calculated by the Runner object.
        """
        self.dataset = dataset
        self.results = results

        # Internal arrays
        self._energy_reference: Optional[np.ndarray] = None
        self._forces_reference: Optional[np.ndarray] = None
        self._energy_hdnnp: Optional[np.ndarray] = None
        self._forces_hdnnp: Optional[np.ndarray] = None
        self._natoms: Optional[np.ndarray] = None
        self._elements: Optional[List[str]] = None

        # Internal mask for selecting subsets of `self.dataset` for analysis.
        self.mask: Optional[List[bool]] = None

    @property
    def natoms(self) -> np.ndarray:
        """Retrieve an array with the number of atoms in each structure."""
        if self._natoms is None:
            natoms = np.array([len(i) for i in self.dataset])

            if self.mask is not None:
                natoms = natoms[self.mask]

            self._natoms = natoms

        return self._natoms.copy()

    @property
    def elements(self) -> List:
        """Retrieve a list with all elements in the dataset."""
        if self._elements is None:
            self._elements = get_elements(self.dataset)

        return self._elements

    @property
    def energy_reference(self) -> np.ndarray:
        """Retrieve the reference energies in `self.dataset`."""
        if self._energy_reference is None:
            energies = []
            for idx, structure in enumerate(self.dataset):
                # Skip points for which mask is False.
                if self.mask is not None:
                    if self.mask[idx] is False:
                        continue

                energies.append(structure.get_potential_energy())

            self._energy_reference = np.array(energies)

        return self._energy_reference.copy()

    @property
    def energy_per_atom_reference(self) -> np.ndarray:
        """Retrieve the reference energies in `self.dataset` per atom."""
        return self.energy_reference / self.natoms

    @property
    def energy_hdnnp(self):
        """Retrieve the energy predictions of the HDNNP."""
        if self._energy_hdnnp is None:
            if 'energy' not in self.results or self.results['energy'] is None:
                raise RuntimeError('Cannot access energy prediction before '
                                   + 'running mode 3.')

            energies = []
            for idx in range(len(self.dataset)):
                # Skip points for which mask is False.
                if self.mask is not None:
                    if self.mask[idx] is False:
                        continue

                # Skip points which were not calculated because RuNNer failed.
                if idx >= len(self.results['energy']):
                    val = np.nan
                else:
                    val = self.results['energy'][idx]

                energies.append(val)

            self._energy_hdnnp = np.array(energies)

        return self._energy_hdnnp.copy()

    @property
    def energy_per_atom_hdnnp(self) -> np.ndarray:
        """Retrieve the energy predictions in `self.dataset` per atom."""
        return self.energy_hdnnp / self.natoms

    @property
    def forces_reference(self):
        """Show the reference forces in `self.dataset`."""
        if self._forces_reference is None:
            forces = []
            for idx, structure in enumerate(self.dataset):
                # Skip points for which mask is False.
                if self.mask is not None:
                    if self.mask[idx] is False:
                        continue

                forces.append(structure.get_forces())

            self._forces_reference = np.array(forces, dtype='object')

        return self._forces_reference.copy()

    @property
    def forces_hdnnp(self):
        """Show the force predictions of the HDNNP."""
        if self._forces_hdnnp is None:
            if 'forces' not in self.results or self.results['forces'] is None:
                raise RuntimeError('Cannot access force prediction before '
                                   + 'running mode 3.')

            forces = []
            for idx, struc in enumerate(self.dataset):
                # Skip points for which mask is False.
                if self.mask is not None:
                    if self.mask[idx] is False:
                        continue

                # Skip points which were not calculated because RuNNer failed.
                if idx >= len(self.results['forces']):
                    val = np.full((len(struc), 3), np.nan)
                else:
                    val = self.results['forces'][idx]

                forces.append(val)

            self._forces_hdnnp = np.array(forces, dtype=object)

        return self._forces_hdnnp.copy()

    def calc_rmse_mae(
        self,
        silent: bool = False,
    ) -> Tuple[float, float, float, float]:
        """Calculate RMSE and MAE for `self.dataset` applying `self.mask`.

        Parameters
        ----------
        silent:
            If True, no information is printed to stdout.

        Returns
        -------
        rmses:
            A tuple containing the energy RMSE, energy MAE, force RMSE, and
            FORCE MAE, in that order.
        """
        if 'energy' not in self.results:
            raise RuntimeError('Please run mode 3 before calculating RMSE.')

        # Get energies in meV/atom.
        energy_hdnnp = self.energy_per_atom_hdnnp * 1000
        energy_reference = self.energy_per_atom_reference * 1000

        # Get forces in meV/A.
        forces_hdnnp = self.forces_hdnnp.flatten() * 1000
        forces_reference = self.forces_reference.flatten() * 1000

        # Calculate RMSE and MAE.
        rmse_enery = calc_rmse(energy_hdnnp, energy_reference)
        mae_energy = calc_mae(energy_hdnnp, energy_reference)
        rmse_forces = calc_rmse(forces_hdnnp, forces_reference)
        mae_forces = calc_mae(forces_hdnnp, forces_reference)

        # Print results.
        if silent is False:
            print(
                f'E (meV/atom): n={len(energy_hdnnp)}; '
                f'RMSE: {rmse_enery:.2f}; '
                f'MAE: {mae_energy:.2f}'
            )
            print(
                f'F (meV/Ang): n={len(forces_hdnnp)}; '
                f'RMSE: {rmse_forces:.2f}; '
                f'MAE: {mae_forces:.2f}'
            )

        return rmse_enery, mae_energy, rmse_forces, mae_forces

    def binary_convex_hull(self) -> defaultdict:
        """Map out the convex hull for the current `self.dataset`.

        Returns
        -------
        convex_hull:
            For each unique composition in the dataset, returns the
            minimum energy per atom.
        """
        convex_hull = defaultdict(list)

        if len(self.elements) > 2:
            raise RuntimeError('Convex hull calculation is only implemented '
                               'for binary systems.')

        for struc in self.dataset:
            comp = binary_composition(struc, self.elements[0])
            energy = struc.get_potential_energy() / len(struc)
            convex_hull[comp].append(energy)

        for comp, energies in convex_hull.items():
            convex_hull[comp] = min(energies)

        return convex_hull

    def get_atomic_energies(self) -> Dict[str, float]:
        """Get atomic energies minimizing the energy range in `self.dataset`.

        From all structures in the dataset that contain all elements at once,
        this routine chooses the structure with the minimum energy and then
        optimizes per-element atomic energies such that their sum batches
        the total energy of that structure as closely as possible.
        Be warned: if your dataset does not contain a structure with all
        elements, this routine will return 0.
        """
        energies = self.energy_reference

        # Get the structure with the minimum energy that contains all elements.
        mask = []
        elements = get_elements(self.dataset)
        for struc in self.dataset:
            mask.append(all(i in struc.symbols for i in elements))

        min_energy_idx = abs(energies[mask]).argmin()

        return optimize_atomic_energies(self.dataset[min_energy_idx])

    def energy_absolute_mean(
        self,
        calcs: List["RunnerAnalysis"]
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Calculate mean/std dev of the predicted energy for many potentials.

        Parameters
        ----------
        calcs:
            The list of RunnerAnalysis objects (each holding one trained
            potential) for which the results are calculated.

        Returns
        -------
        tuple:
            The mean and standard deviation of the predicted energy per atom
            of all potentials.

        """
        energies = []

        for calc in [self] + calcs:
            energies.append(calc.energy_per_atom_hdnnp)

        means = np.mean(energies, axis=0)
        stds = np.std(energies, axis=0)

        return means, stds

    def energy_error_mean(
        self,
        calcs: List["RunnerAnalysis"]
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Calculate pred. energy error mean/std dev for a list of potentials.

        Parameters
        ----------
        calcs:
            The list of RunnerAnalysis objects (each holding one trained
            potential) for which the results are calculated.

        Returns
        -------
        tuple:
            The mean and standard deviation of the difference between the
            predicted energy per atom and the reference of all potentials.
        """
        energies = []
        for calc in [self] + calcs:
            ediff = calc.energy_per_atom_hdnnp - self.energy_per_atom_reference
            energies.append(ediff)

        means = np.mean(energies, axis=0)
        stds = np.std(energies, axis=0)

        return means, stds

    def forces_absolute_mean(
        self,
        calcs: List["RunnerAnalysis"]
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Calculate pred. force comp mean/std dev for a list of potentials.

        Parameters
        ----------
        calcs:
            The list of RunnerAnalysis objects (each holding one trained
            potential) for which the results are calculated.

        Returns
        -------
        tuple:
            The mean and standard deviation of the predicted force components
            of all potentials.
        """
        forces = []
        for calc in [self] + calcs:
            forces.append(calc.forces_hdnnp.flatten().astype(np.float64))

        means = np.mean(forces, axis=0)
        stds = np.std(forces, axis=0)

        return means, stds

    def forces_error_mean(
        self,
        calcs: List["RunnerAnalysis"]
    ) -> Tuple[np.ndarray, np.ndarray]:
        """Calculate pred. force comp. error mean/std dev for many potentials.

        Parameters
        ----------
        calcs:
            The list of RunnerAnalysis objects (each holding one trained
            potential) for which the results are calculated.

        Returns
        -------
        tuple:
            The mean and standard deviation of the difference between the
            predicted force components and the reference of all potentials.
        """
        forces = []
        force_ref = self.forces_reference.flatten().astype(np.float64)
        for calc in [self] + calcs:
            forces_calc = calc. forces_hdnnp.flatten().astype(np.float64)
            fdiff = force_ref - forces_calc
            forces.append(fdiff)

        means = np.mean(forces, axis=0)
        stds = np.std(forces, axis=0)

        return means, stds
