#!/usr/bin/env python3
# encoding: utf-8
"""Implementation of LAMMPS simulation protocol."""

from typing import List, Optional, Union

import os

import numpy as np

from ase.atoms import Atoms
from ase.calculators.lammpslib import LAMMPSlib

from runnerase.calculators.runner import Runner
from runnerase.storageclasses import RunnerExtrapolationWarnings


# pylint: disable=too-many-arguments, too-many-locals
# Reason: The workflow is supposed to give the user many options
# for customization to be generally applicable.
def run_nvt_simulation(
    calc: Runner,
    initial_structure: Atoms,
    directory: str,
    rng: np.random.Generator,
    output_freq: int = 10,
    thermo_style: str = 'custom step temp pe etotal vol pxx',
    temp: Union[float, List[float]] = 300.0,
    thermo_damping: float = 100,
    num_steps: int = 1000,
    timestep: float = 5e-4,
    elements: Optional[List[str]] = None,
    **kwargs
):
    r"""Run a NVT MD simulation with LAMMPS.

    This workflow makes use of the LAMMPSlib calculator in ASE, which
    in turn is based on the interactive LAMMPS-python interface.
    It runs a simple NVT simulation with a Nosé-Hoover thermostat and an
    optional temperature ramp. The workflow can serve as an extensible
    blueprint for many other simulation protocols.

    Parameters
    ----------
    calc:
        A trained RuNNer calculator which can supply a finished potential.
    initial_structure:
        The starting structure of the MD simulation.
    directory:
        Directory for running the simulation.
    rng:
        A random number generator initializing the velocities in a reproducible
        fashion.
    output_freq:
        How often trajectory and thermodynamic data is written.
    thermo_style:
        Format of the thermodynamic data written at each `output_freq`
        timestep. by default, step number, temperature,
        potential energy, total energy, volume and pressure are written.
    temp:
        The simulation temperature. Can either be given as a single value
        temperature stays constant or as a list (applies a ramp).
    thermo_damping:
        Damping factor of the thermostat.
    num_steps:
        Number of simulation steps to run.
    timestep:
        Simulation time step. Units are the LAMMPS metalic units by default.
    elements:
        List of elements for `calc.get_lammps_potential`. Not strictly
        required, but it is highly encouraged to set this by hand to make sure
        that it is right.
    **kwargs:
        All kwargs are passed on to the get_lammps_potential method of the
        calculator.

    Returns
    -------
    extrapolations:
        A summary of all extrapolation warnings that occured during the
        simulation.
    """
    logfile = os.path.join(directory, 'lammps.log')
    dumpfile = os.path.join(directory, 'dump.lammpstrj')
    initialfile = os.path.join(directory, 'initial.data')

    if not os.path.exists(directory):
        os.makedirs(directory)

    if isinstance(temp, (float, int)):
        temp = [temp, temp]

    # Create a LAMMPS calculation based on the RuNNer fit.
    cmds = calc.get_lammps_potential(elements=elements, **kwargs)

    # Setup the system.
    atoms = initial_structure.copy()

    # Create and initialize a LAMMPS calculator.
    lmp = LAMMPSlib(
        lmpcmds=cmds,
        log_file=logfile,
        keep_alive=False
    )
    lmp.start_lammps()
    lmp.initialise_lammps(atoms)

    # Convert the box dimensions.
    lmp.lmp.command(f'change_box all boundary {lmp.lammpsbc(atoms)}')
    lmp.set_lammps_pos(atoms)

    # Write the initial structure once for analysis.
    lmp.lmp.command(f'write_data {initialfile}')

    # Define thermo and traj output.
    lmp.lmp.command(f'thermo {output_freq}')  # type: ignore
    lmp.lmp.command(f'thermo_style {thermo_style}')  # type: ignore
    lmp.lmp.command(f'dump 1 all atom {output_freq} {dumpfile}')  # type: ignore

    # Create thermostat and initial velocity distribution.
    lmp.lmp.command(  # type: ignore
        f'fix 1 all nvt temp {temp[0]} {temp[1]} {thermo_damping}'
    )
    lmp.lmp.command(  # type: ignore
        f'velocity all create {temp[0]} {int(rng.integers(1, 100000))}'
    )

    lmp.lmp.command(f'timestep {timestep}')

    # Run MD.
    try:
        lmp.lmp.command(f'run {num_steps}')
    except Exception as err:  # pylint: disable=broad-exception-caught
        print('Simulation aborted with exception: ', err)

    # Get the extrapolation warnings.
    extrapolations = RunnerExtrapolationWarnings(logfile)

    return extrapolations
