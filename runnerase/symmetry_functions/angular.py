"""Implementation of classes for storing RuNNer parameters and results.

This module provides custom classes for storing the different types of data
produced and/or read by RuNNer.

Attributes
----------
SymmetryFunction:
    Storage container for a single symmetry function.
SymmetryFunctionSet:
    Storage container for a collection of symmetry functions.
get_element_groups:
    Create a list of element pairs and triples from a list of chemical symbols.
get_minimum_distances:
    Find the minimum distance for each pair of elements in a list of images.
"""

from typing import Optional, Dict, List, Union

import numpy as np

from ase.calculators.calculator import CalculatorSetupError
from ase.atoms import Atoms

from runnerase.utils.atoms import get_element_groups, get_triplets

from .symmetryfunctions import SymmetryFunctionSet, SymmetryFunction

# Typical zeta coefficients for angular symmetry functions in the literature.
LITERATURE_ANGULAR_COEFFICIENTS = [1.0, 2.0, 4.0, 16.0, 32.0, 64.0, 128.0]


# pylint: disable=too-many-arguments, too-many-branches, too-many-locals
# Reason: symmetry function have many options and arguments.
def generate_symmetryfunctions_sftype3(
    parent_symfunset: SymmetryFunctionSet,
    elements: List[str],
    dataset: Optional[List[Atoms]] = None,
    cutoff: float = 10.0,
    amount: int = 6,
    algorithm: str = 'half',
    lambda_angular: Optional[List[float]] = None,
    eta_angular: Optional[Union[Dict[str, float], SymmetryFunctionSet]] = None
) -> None:
    """Generate a set of type 3 symmetry functions."""
    if isinstance(eta_angular, SymmetryFunctionSet):
        all_etas: Dict[str, List[float]] = {}
        for symfun in eta_angular:

            # Check that the symmetry function has a coefficient.
            if symfun.coefficients is None or symfun.elements is None:
                raise CalculatorSetupError(
                    f'SymmetryFunction {symfun} is not defined.'
                )

            label = '-'.join(symfun.elements)
            eta = symfun.coefficients[0]

            if label not in all_etas:
                all_etas[label] = []
            all_etas[label].append(eta)

        eta_angular = {}
        for label, etas in all_etas.items():
            eta_angular[label] = sorted(etas)[-2]

    triplets_in_dataset = ['-'.join(i) for i in get_element_groups(elements, 3)]
    if dataset is not None:
        triplets_in_dataset = get_triplets(elements, dataset, cutoff)

    # Create one set of symmetry functions for each element triplet.
    for element_group in get_element_groups(elements, 3):

        eta_list = [0.0]

        if '-'.join(element_group) not in triplets_in_dataset:
            continue

        # Choose the right radial eta values for this triplet, if provided.
        if eta_angular is not None and algorithm == 'eta_mean':
            radial_etas = []
            for pair in get_element_groups(element_group, 2):
                label = '-'.join(pair)
                radial_etas.append(eta_angular[label])

            mean_eta: float = float(np.mean(radial_etas))
            eta_list += [mean_eta]

        if lambda_angular is None:
            lambda_angular = [-1.0, +1.0]

        for lamb in lambda_angular:
            for eta in eta_list:
                # Add `amount` symmetry functions to a fresh set.
                element_symfunset = SymmetryFunctionSet()
                for _ in range(amount):
                    element_symfunset += SymmetryFunction(
                        sftype=3,
                        cutoff=cutoff,
                        elements=element_group
                    )

                # Set the symmetry function coefficients. This modifies
                # symfunset.
                _set_angular_coefficients(element_symfunset, algorithm, lamb,
                                          eta)

                parent_symfunset.append(element_symfunset)


def _set_angular_coefficients(
    sfset: SymmetryFunctionSet,
    algorithm: str,
    lambda_angular: float,
    eta_angular: float = 0.0
) -> None:
    """Calculate the coefficients for a set of angular symmetry functions."""
    # Calculate the angular range that has to be covered.
    interval = 160.0 / len(sfset)

    for idx, symfun in enumerate(sfset.symmetryfunctions):
        turn: float = (160.0 - interval * idx) / 180.0 * np.pi

        # Equally spaced at G(r) = 0.5.
        if algorithm == 'half':
            symfun.coefficients = _calc_coeff_half(turn, lambda_angular,
                                                   eta_angular)

        # Equally spaced turning points.
        elif algorithm == 'turn':
            symfun.coefficients = _calc_coeff_turn(turn, lambda_angular,
                                                   eta_angular)

        # Library of literature values or algorithm based on mean of radial
        # eta values.
        elif algorithm in ['literature', 'eta_mean']:
            symfun.coefficients = [eta_angular, lambda_angular, 2**idx]

        elif algorithm == 'legacy':

            if idx > 5:
                raise NotImplementedError('Legacy algorithm only works for up '
                                          + 'to 5 angular symmetry functions '
                                          + 'per triplet.')

            symfun.coefficients = [
                eta_angular,
                lambda_angular,
                LITERATURE_ANGULAR_COEFFICIENTS[idx]]

        else:
            raise NotImplementedError(f"Unknown algorithm '{algorithm}'.")


def _calc_coeff_turn(
    turn: float,
    lamb: float,
    eta: float
) -> List[float]:
    """Calculate `zeta` such that the symfun turning point is `turn`."""
    costurn: float = np.cos(turn)
    sinturn: float = np.sin(turn)
    rho = 1.0 + lamb * costurn
    zeta = 1.0 + (costurn / sinturn**2) * rho / lamb

    return [eta, lamb, zeta]


def _calc_coeff_half(
    turn: float,
    lamb: float,
    eta: float
) -> List[float]:
    """Calculate `zeta` such that the symfun half value point is `turn`."""
    costurn: float = np.cos(turn)
    rho = 1.0 + lamb * costurn
    logrho: float = np.log(rho)
    zeta: float = -np.log(2) / (logrho - np.log(2))

    return [eta, lamb, zeta]
